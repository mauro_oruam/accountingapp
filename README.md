# accountingapp 

This project was developed for the Magoya Challenge.
It consists in a webapp for personal accounting management. You can load your transactions, with their type and description, and it will show your account balance and a chart of it's evolution in time. 

## Installing

Clone the repo from Gitlab to your workspace:

$ git clone git@gitlab.com:mauro_oruam/accountingapp.git

Navigate into the repo folder:

$ cd accountingapp

Create a virtual enviroment inside the project folder and activate it:

$ virtualenv .
$ source bin/activate

Install the requirements:

$ pip install -r requirements.txt

Navigate inside the frontend folder, install the NPM modules and initialize the webpack:

$ npm install
$ npm run dev

Go back to the main folder and execute migrations:

$ python manage.py makemigrations
$ python manage.py migrate

Initialize the Django server:

$ python manage.py runserver




## Built with

* [Django](https://www.djangoproject.com/) 
* [Django Rest Framework](https://www.django-rest-framework.org/) 
* [React](https://es.reactjs.org/) 
* [Bootstrap](https://getbootstrap.com/) 


## Author

* **Mauro Agustín Bagnoli** - *More works* - [Portfolio](https://maurobagnoli.github.io/resume)


## Licence

This project is under a MIT licence - see [license](https://opensource.org/licenses/MIT) for more details. 

