from django.db import models
import datetime
from django.core.validators import MinValueValidator

# Create your models here.

TRANSACTION_TYPES = (
            ('DB','debit'),
            ('CR','credit'),
            )

class Transaction(models.Model):
  description = models.CharField(max_length=200)
  type = models.CharField(choices=TRANSACTION_TYPES,max_length=2, default='DB')
  created_at = models.DateTimeField(default=datetime.datetime.now)
  amount =  models.DecimalField(max_digits=200, decimal_places=2, validators = [MinValueValidator(0.1)])

  def __str__(self):
    return self.description
