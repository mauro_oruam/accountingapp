import React, { Component } from "react";
import { render } from "react-dom";
import { styles } from './styles.js'

class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: null,
      description:null,
      type:'CR',
      amount:null,
      balance: 0,
      buttonDisabled: true
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getCookie = this.getCookie.bind(this);
  }

  componentDidMount(){
    this.setState({balance:this.props.location.state.fromTransactions})
  }

  getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
  }

  handleChange (event) {
    this.setState({
      [event.target.name]: event.target.value,
    });

  };

  isDisabled() {
    if(!this.state.description || !this.state.type || !this.state.amount
    || ((this.state.amount > this.state.balance) && this.state.type == 'DB')){
      return true
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    let item =  this.state;
    if ( (item["amount"] > item["balance"]) && this.state.type == 'DB' ) {
      alert('Not enough funds')
    }
    else{
    delete item ["balance"]
    delete item ["balanceWarning"]
    delete item ["buttonDisabled"]
    var csrftoken = this.getCookie('csrftoken')

    fetch("api/transaction-create/",
      {
        method: 'POST',
        body: JSON.stringify(item),
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken':csrftoken,
        }
      }
    );
    this.props.history.push('');
  }
}

  render () {
    return (
      <form className="container" onSubmit={this.handleSubmit}
        style={styles.container}>
      <h1>New Transaction</h1>
        <div className="form-group">
          <label htmlFor="amount">Amount</label>
          <input onChange={this.handleChange} type="number" step="0.01"
            min="0" className="form-control"
            id="amount" aria-describedby="amount" name="amount" required/>
        </div>
        <div className="form-group">
        <label htmlFor="type">Transaction type</label>
        <select onChange={this.handleChange} className="form-control"
          name="type" required>
            <option value="CR">Credit</option>
            <option value="DB">Debit</option>
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="description">Description</label>
          <input onChange={this.handleChange} type="text"
            className="form-control"
            id="description" name="description" required />
        </div>
        <div>
        {this.state.type == 'DB' && (this.state.balance < this.state.amount) ? (
          <div className="alert alert-danger" role="alert">
          Not enough funds
        </div>
        ) : (
      null
    )}
        </div>
        <button type="submit" className="btn btn-primary"
          disabled={this.isDisabled()}>Submit</button>
        <h2 style={styles.container}> Balance: ${ this.state.balance }</h2>
      </form>
    )
  }
}
export default Form;
