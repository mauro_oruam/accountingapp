import React, { Component } from "react";
import { Link } from 'react-router-dom'
import { render } from "react-dom";
import { styles } from './styles.js'
import { link } from 'react-router-dom'
import { BsCaretDownFill } from "react-icons/bs";
import { BsCaretUpFill } from "react-icons/bs";
import { IcBsFillTrashFillonName } from "react-icons/bs";
import Chart from './Chart.js'


class Transactions extends Component{
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loaded: false,
      balance: 0
    };
    this.calculate_balance = this.calculate_balance.bind(this)
  }

  componentDidMount() {
    fetch("api/transaction-list")
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
        };
      });
      this.calculate_balance(data)
    });
  }

  calculate_balance(data) {
    let total = 0
    total = data.map(transaction => {
      (transaction.type )== 'DB' ?
      total -= parseFloat(transaction.amount) :
      total += parseFloat(transaction.amount)
      this.setState({ balance:total })
    })
  }

  render () {
    let data = [...this.state.data].reverse()
    return !this.state.loaded ? (
      <div style={styles.loaderContainer}>
      <div className="spinner-border text-success" style={styles.loader} role="status">
      </div>
      </div>)
      : (
      <div className="container"  style={ styles.container }>
        <div className="row">
          <div className="col-lg-6 col-sm-12">
          <h1 style={ styles.balance }>Balance: ${ this.state.balance }</h1>
          <Link style={styles.button} className="btn btn-outline-dark" type="submit"
            to={{ pathname:"/new-transaction", state:{ fromTransactions:this.state.balance }}}>
            New transaction
          </Link>
        </div>
        <div className="col-lg-6 col-sm-12">
          <Chart data={ this.state.data }/>
        </div>
      </div>
      <div className="row">
          <div className="col-lg-12">
            <h2>Transactions</h2>
              <table className="table table-striped table-sm" >
                <thead>
                  <tr>
                  <th>Date</th>
                  <th>Description</th>
                  <th>Amount</th>
                  <th>Type</th>
                </tr>
                </thead>
                <tbody>
                  {data.map(transaction => {
                    let date = moment(transaction.created_at);
                    date = date.format('DD-MM-YYYY')
                    let amount = transaction.amount.replace(/\.00$/,'');
                    return (
                    <tr key={ transaction.id }>
                      <td style={ styles.td }>{ date }</td>
                      <td style={ styles.td }>{ transaction.description }</td>
                      <td style={ styles.td }>$ { amount }</td>

                      {(transaction.type) == 'DB' ?
                      (<td style={ { color:'red', verticalAlign: "middle" }}>
                        Debit <BsCaretDownFill /></td>) :
                      (<td style={{ color:'green', verticalAlign: "middle" }}>
                        Credit<BsCaretUpFill /></td>)}
                    </tr>
                  );
                })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
    )

  }
}
export default Transactions;
