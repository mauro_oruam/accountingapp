import React, { Component } from "react";
import { Route } from "react-router-dom";
import Transactions from "./Transactions.js";
import Form from "./Form.js";


class Main extends Component {
  render () {
    return (
      <div>
        <Route exact path="/" component={Transactions} />
        <Route path="/new-transaction" component={Form} />
      </div>
    )
  }
}
export default Main;
