import React from "react";

export const styles = {
  list: {
    margin: '40px',
    border: '5px solid pink'
  },
  debit: {
    backgroundColor: "whitesmoke"
  },
  credit: {
    backgroundColor: "lightgray"
  },
  container: {
    paddingTop: 50
  },
  transac: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  balance: {
    paddingTop: 50,
    marginRight: '10px'
  },
  chartHeight: {
    width: "auto",
    height: '300px'
  },
  title: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between',
    alignContent: 'center',
    marginBottom: '30px'
  },
  titleh1: {
    marginBottom: '0px'
  },
  total: {
    display: 'inline-block'
  },
  gridcontainer: {
    display: 'grid',
    gridGap: '20px 20px',
    gridTemplateColumns: 'auto auto auto',
    padding: '10px'
  },
  griditem: {
    padding: '20px',
    textAlign: 'center'
  },
  date: {
    marginBottom: '0px'
  },
  td: {
    verticalAlign: "middle"
  },
  footer: {
    background: '#152F4F',
    color: 'white'
  },
  nav: {
    background: '#152F4F',
    color: 'white'
  },
  links: {
    color: 'white',
    listStyleType: "none",
    textDecoration: 'none'
  },
  aboutCompany: {
    fontSize: '25px',
    color: 'white'
  },
  location: {
    fontSize: '18px'
  },
  copyright: {
    borderTop: '1px solid rgba(255,255,255,.1)'
  },
  loaderContainer: {
    height: "1200px",
    width: "100%",
    display: "block"
  },
  loader: {
    position: "fixed",
    zIndex: 999,
    height: "2em",
    width: "2em",
    overflow: "visible",
    margin: "auto",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  button: {
    marginTop: "15px"
  }
}
