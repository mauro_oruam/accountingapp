import React, { Component } from "react";
import { Link } from 'react-router-dom'
import { render } from "react-dom";
import { styles } from './styles.js'
import ZingChart from 'zingchart-react';

class Chart extends Component{


  render() {
    let total = 0
    let vals = this.props.data.map(transaction => {
      if (transaction.type == 'DB') {
        total -= parseFloat(transaction.amount)
      }else{
        total += parseFloat(transaction.amount)
      }
      return (total)
    })

    let dates = this.props.data.map(transaction => {
      return (moment(transaction.created_at).format('DD-MM'))
    })
    let config = {
      type: 'line',
      scaleX: {
        values: dates,
      },
      series: [{
        values: vals
      }],
      height: '300px',
    }
    return (
      <div style={styles.chartHeight}>
        <ZingChart data={config}/>
      </div>
    );
  }
}

export default Chart;
