import React, { Component } from "react";
import { Link } from 'react-router-dom'
import { render } from "react-dom";
import { styles } from './styles.js'


class Footer extends Component{

  render (props) {
    return (
      <div className="mt-5 pt-5 pb-2 " style={styles.footer}>
        <div className="container">
          <div className="row">
            <div className="col-lg-5 col-xs-12 " style={styles.aboutCompany}>
              <h2>Accounting App</h2>

            </div>
            <div className="col-lg-3 col-xs-12 " style={styles.links}>
              <h4 className="mt-lg-0 mt-sm-3">Links</h4>
                <ul className="m-0 p-0">
                  <li style={styles.links}><a  style={styles.links} href="https://maurobagnoli.github.io/resume/" target="_blank">Portfolio</a></li>
                </ul>
            </div>
            <div className="col-lg-4 col-xs-12 " style={styles.location}>
              <h4 className="mt-lg-0 mt-sm-4">License</h4>
              <p><a  style={styles.links} href="https://opensource.org/licenses/MIT" target="_blank">MIT License</a></p>
            </div>
          </div>
          <div className="row mt-5">
            <div className="col" style={styles.copyright}>
              <p className=""><small className="text-white-50">Developed by Mauro Bagnoli.</small></p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default Footer;
